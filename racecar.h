#ifndef RACECAR_H
#define RACECAR_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>

class Racecar {
public:
    Racecar(float maxSpeed, float acceleration, float steerSpeed, sf::Color color);
    void update(float dt);
    void accelerate(float dt);
    void boost(float dt);
    void decelerate(float dt);
    void brake(float dt); // Brake the car
    void stop();
    void reverse(float dt); // Reverse the car
    void steer(float dt, float direction); // -1 for left, 1 for right
    sf::Text getDrawable();
    void setPosition(float x, float y);
    void setRotation(float value);
    float getRotation();
    float getSpeed() const; // Get the current speed of the car
    float getX() const;
    float getY() const;

private:
    sf::Text carText;
    sf::Font font;
    float speed;
    float maxSpeed;
    float acceleration;
    float steerSpeed;
    float steeringAngle;
    float dx;
    float dy;
};

#endif // RACECAR_H

