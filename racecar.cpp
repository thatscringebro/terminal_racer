#include "racecar.h"

Racecar::Racecar(float maxSpeed, float acceleration, float steerSpeed, sf::Color color)
    : maxSpeed(maxSpeed), acceleration(acceleration), steerSpeed(steerSpeed), dx(0), dy(0), steeringAngle(3.13) {
    if (!font.loadFromFile("cascaydia.otf")) {
        std::cerr << "Failed to load font" << std::endl;
        exit(1);
    }

    carText.setString("|:=:>"); // Replace "o" with a racecar emoji or any desired character
    carText.setFont(font);
    carText.setCharacterSize(12);
    carText.setFillColor(color);
    
    speed = 0;
}

void Racecar::update(float dt) {
    carText.move(speed * dt * cos(steeringAngle), speed * dt * sin(steeringAngle));
}

void Racecar::accelerate(float dt) {
    if (speed < maxSpeed) {
        speed += acceleration * dt;
    }
    else {
        decelerate(dt);
    }
}

void Racecar::decelerate(float dt) {
    if (speed < -1) {
        speed += 3 * acceleration * dt;
    } else if (speed > 1) {
        speed -= 3 * acceleration * dt;
    }
    else {
        speed = 0;
    }
}

void Racecar::steer(float dt, float direction) {
    steeringAngle += steerSpeed * direction * dt;
    carText.setRotation(steeringAngle * 180 / M_PI);
}

void Racecar::brake(float dt) {
    if (speed < -1) {
        speed += 9 * acceleration * dt;
    } else if (speed > 1) {
        speed -= 9 * acceleration * dt;
    }
    else {
        speed = 0;
    }
}

void Racecar::stop() {
    speed = 0;
}

void Racecar::reverse(float dt) {
    if (speed > -maxSpeed / 2) {
        speed -= 1.5 * acceleration * dt;
    }
}

sf::Text Racecar::getDrawable() {
    return carText;
}

void Racecar::setPosition(float x, float y) {
    carText.setPosition(x, y);
}

void Racecar::setRotation(float value) {
    carText.setRotation(value);
}

float Racecar::getRotation() {
    return steeringAngle;
}

float Racecar::getSpeed() const {
    return speed;
}

float Racecar::getX() const {
    return carText.getPosition().x;
}

float Racecar::getY() const {
    return carText.getPosition().y;
}

void Racecar::boost(float dt) {
    if (speed < maxSpeed * 2) {
        speed += (acceleration * dt) * 5;
    }
}
