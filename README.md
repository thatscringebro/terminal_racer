# Terminal racer

## Description

This is a simple game made in c++ that is entirely displayed in text. It's a racing game inspired by retro style games. 

## Setup

You may build the game with g++ and then run the executable. You need sfml in order to run it
This command should build and run the game 
```
g++ *.cpp -lsfml-graphics -lsfml-window -lsfml-system -o terminal_racer && ./terminal_racer
```

## Usage

The default kays are the arrows on your keypad.


## Developper
|Name|GitLab|
|--|--|
|Merlin Gelinas|thatscringebro|

<sub>This game is entirely free of use, please share it! =^◡^=</sub>
