#include <SFML/Graphics.hpp>
#include <iostream>
#include "game.h"
#include "racecar.h"

int main(int argc, char** argv) {
    Game game;
    game.run();

    return 0;
}
