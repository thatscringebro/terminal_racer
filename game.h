#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include "racecar.h"

class Game {
public:
    Game();
    void run();
    bool isTrackCollision(float x, float y);
    int determineQuadrant(float x, float y);
    void displaySavedTimes();
    void determineLap(int millis, std::string timerString);
    void displayEndGame();
    void loadHighScores();
    void writeHighScores();
    void loadPlayerMovesFromFile(const std::string& filename);
    void botMove(int i, float dt);

private:
    int max_x;
    int max_y;
    int direction;
    sf::RenderWindow window;
    sf::Font font;
    sf::Text speedText;
    sf::Text nitroText;
    sf::Text mapText;
    sf::Text timerText;
    sf::Clock gameTimeClock;
    sf::View view;
    Racecar racecar;
    Racecar bot;
    std::vector<int> lastPlayerMoves;
    std::vector<std::pair<int, std::string>> timesAndQuadrants; // Store times and quadrants
    bool lapFinished;
    unsigned int lastRound = 0;
    const std::string highScoresFile = "high_scores.txt";
    std::vector<int> highScores;
    std::vector<unsigned int> lapPoints; // Store lap times
    float modifier = 1.0f;
    sf::Music backgroundMusic;
    int nitro = 300;
    std::string asciiMap = "        /------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\\    \n"
                           "       /                                                                                                                                                                                      \\  \n"
                           "      /                                                                                                                                                                                        | \n"
                           "     /                                                                                                                                                                                         | \n"
                           "    /             _____________________________________________________________________________________________________________________________________________________________                | \n"
                           "   /             /                                                                                                                                                             \\               | \n"
                           "  /             /                                                                                                                                                               |              | \n"
                           " /             |  /------------------------------------------------------\\                                                                                                     /               | \n"
                           "/              | /                                                        \\                 _____    __________________ .___.___                                              /               /  \n"
                           "|              ||                                                          |               /  _  \\  /   _____/\\_   ___ \\|   |   |                                            /               /   \n"
                           "|              ||                                                          |              /  /_\\  \\ \\_____  \\ /    \\  \\/|   |   |                                           /               /    \n"
                           "|              ||                 __________________________               |             /    |    \\/        \\\\     \\___|   |   |                                          /               /     \n"
                           "|              ||                /                          \\              |             \\____|__  /_______  / \\______  /___|___|                                         /               /      \n"
                           "|              ||               /                            |             |                     \\/        \\/         \\/                                                 /               /       \n"
                           "|              ||               |                            |             |             __________    _____  _________ _____________________                           /               /        \n"
                           "|              ||               |                           /             /              \\______   \\  /  _  \\ \\_   ___ \\_   _____/\\______   \\                          /               /         \n"
                           "|              ||               |                          /             /                |       _/ /  /_\\  \\/    \\  \\/ |    __)_  |       _/                        /               /          \n"
                           "|              ||               |                         /             /                 |    |   \\/    |    \\     \\____|        \\ |    |   \\                       /               /           \n"
                           "|              ||               |                        /             /                  |____|_  /\\____|__  /\\______  /_______  / |____|_  /                      /               /            \n"
                           "|              ||               |                       /             /                          \\/         \\/        \\/        \\/         \\/                      /               /             \n"
                           "|              ||               |                      /             /                                                                                            /               /              \n"
                           "|              ||               |                     /             /                                                                                            /               /               \n"
                           "|              ||               |                    /             /                                                                                            /               /                \n"
                           "|              ||               |                   |              |                                                                                           /               /                 \n"
                           "|              ||               |                   |              |                                                                                          /               /                  \n"
                           "|              ||               |                    \\              \\                                                                                        /               /                   \n"
                           "|              ||               |                     \\              \\                                                                                      /               /                    \n"
                           "\\              \\/              /                       \\              \\____________________________________________________________________________________/               /                     \n"
                           " \\                            /                         \\                                                                                                                 /                      \n"
                           "  \\                          /                           \\                                                                                                               /                       \n"
                           "   \\                        /                             \\                                                                                                             /                        \n"
                           "    \\                      /                               \\                                                                                                           /                         \n"
                           "     \\____________________/                                 \\_________________________________________________________________________________________________________/                          ";
};

#endif // GAME_H

